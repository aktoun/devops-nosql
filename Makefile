bold=$(shell (tput bold))
normal=$(shell (tput sgr0))
.DEFAULT_GOAL=help
DISTRIB:=$(shell lsb_release -is | tr '[:upper:]' '[:lower:]')
VERSION:=$(shell lsb_release -cs)
ARCHITECTURE:=$(shell dpkg --print-architecture)

help:
	@echo "${bold}install${normal}\n\t Installs the whole appplication.\n"
	@echo "${bold}uninstall${normal}\n\t Stops and removes all containers and drops the database.\n"
	@echo "${bold}start${normal}\n\t Starts the application.\n"
	@echo "${bold}db.connect${normal}\n\t Connects to the database.\n"
	@echo "${bold}phpunit.run${normal}\n\t Runs the unit tests.\n"

start:
	docker-compose up -d --build
	sleep 3

stop:
	docker-compose down -v
	docker-compose rm -v

install: uninstall start

uninstall: stop
	@sudo rm -rf ./docker/postgres/data
	@sudo rm -rf ./docker/mongodb/data

reinstall: install

#Connects to the databatase
db.connect:
	docker-compose exec postgres /bin/bash -c 'psql -U $$POSTGRES_USER'

dbPostgres.install:
	docker-compose exec postgres /bin/bash -c 'psql -U $$POSTGRES_USER -h localhost -f /docker-entrypoint-initdb.d/db.sql'

dbMongo.install:
	docker-compose exec mongodb /bin/bash -c 'mongo -u $$MONGO_INITDB_ROOT_USERNAME -p $$MONGO_INITDB_ROOT_PASSWORD /docker-entrypoint-initdb.d/mongo-init.js'

php.connect:
	docker-compose exec php /bin/bash

phpunit.run:
	docker-compose exec php vendor/bin/phpunit --config=phpunit.xml

composer.install:
	docker-compose exec php composer install || exit 0
