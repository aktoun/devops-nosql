# noSQL

## Objectifs

Le but de ce projet est de constituer une application web simple, avec au moins une page avec un formulaire qui puisse récupérer des données écrites par un utilisateur et qui les persiste dans au moins deux systemes différents parmi : postgresql - redis - mongodb - neo4j - elasticsearch. 

Le projet doit avoir au moins un fichier docker compose qui orchestre donc au moins 3 docker (un pour la partie server, et un par techno de stockage)

Le format de restitution est un espace sur github - 

Je doit pourvoir git clone, jouer docker compose up pour interagir avec l'interface web, lancer au moins deux terminaux pour me connecter à chacun des systemes de persistance et constater la persistance.

La date limite de publication du projet sur github est le 14 décembre.

Comme discuté nous passerons l'aprés midi du 15 décembre à présenter les projet à la classe pour échanger sur vos réalisations.
Vous pouvez vous mettre par équipe de 2 pour faire ces réalisations, merci de remplir le tableau ci aprés avec vos noms et url de projet

## Commandes Makefile

### make install
Permet : 
- de supprimer les volumes partagés préalablement
- de lancer les containers

### make stop
Permet d'arrêter les containers

### make start
Lancer les containers

### make uninstall
Supprimer les volumes partagés
