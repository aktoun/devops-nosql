<!doctype html>
<html lang="fr">

<?php include('./view/front/head.html'); ?>

<body>

    <?php include('./view/front/header.html'); ?>

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Nouveau compte</h1>
                            </div>

                            <form action="?controller=User&action=registration" method="post">

                                <?php if (isset($data['message_err'])) { ?>

                                    <div class="form-group">
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong><?php echo $data['message_err'] ?></strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                    </div>

                                <?php
                                } 
                                ?>
                                
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                            placeholder="Nom" name="nom" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                                            placeholder="Prénom" name="prenom" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                        placeholder="Email" name="mail" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" name="password" required>
                                </div>
                                <input class="btn btn-primary btn-user btn-block" type="submit" value="Créer compte">
                                <hr>

                            </form>

                            <div class="text-center">
                                <a class="small" href="?controller=User&action=login">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('./view/front/footer.html'); ?>

</body>

</html>
