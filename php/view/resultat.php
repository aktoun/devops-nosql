<?php
    if (isset($_SESSION['user'])) 
    {
        $user = $_SESSION['user'];
    }

    if (isset($_SESSION['res'])) 
    {
        $data = $_SESSION['res'];
    }
?>

<!DOCTYPE html>
<html lang="en">

<?php include('./view/front/head.html'); ?>

<script type="text/javascript">
    
	$(document).ready(function () {
	    var myTable = $('#table_id').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "data": [],
            "columns": [
                { "data": "id" },
                { "data": "DateD" },
                { "data": "GareD" },
                { "data": "GareA" },
                { "data": "Capacite" },
                {
                       "render": function (data, type, JsonResultRow, meta) {
                            return '<form action="?controller=Trains&action=reserver" method="post">'
                            +'<input type="hidden" value="'+JsonResultRow.id+'" name="id">'
                            +'<input type="hidden" value="'+JsonResultRow.quantite+'" name="nbrReservation">'
                            +'<input type="submit" value="Réserver"></form>';
                    }, "orderable": false
                },
            ]
	    });

        var logs = <?php echo $data; ?>;
        myTable.clear();
        $.each(logs, function(index, value) {
            myTable.row.add(value);
        });
        myTable.draw();
	} );
	
</script>

<body id="page-top">

    <?php include('./view/front/header.html'); ?>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('./view/front/sidebar.html'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include('./view/front/topbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">
                
                   	<h1 style="text-align: center;">Liste de trains</h1>
                    
	                    <table id="table_id" class="display">
						    <thead>
						        <tr>
						            <th>ID Train</th>
						            <th>Date de départ</th>
						            <th>Départ</th>
						            <th>Destination</th>
						            <th>Nombre de sièges</th>
                                    <th></th>
						        </tr>
						    </thead>
						</table>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Prêt à nous quitter ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span>
                    </button>
                </div>
                <div class="modal-body">Cliquer sur "Se déconnecter" afin de mettre fin à votre session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                    <a class="btn btn-primary" href="?controller=User&action=logout">Se déconnecter</a>
                </div>
            </div>
        </div>
    </div>

    <?php include('./view/front/footer.html'); ?>

</body>

</html>