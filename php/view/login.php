<!doctype html>
<html lang="fr">

<?php include('./view/front/head.html'); ?>

<body>

    <?php include('./view/front/header.html'); ?>

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Authentification</h1>
                                    </div>
                                    <form action="?controller=User&action=login" method="post">
                                    
                                        <?php if (isset($data['e-message'])) { ?>
                                        
                                            <div class="form-group">
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <strong><?php echo $data['e-message'] ?></strong>
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>

                                        <?php
                                        } 
                                        ?>

                                        <?php if (isset($data['message_success'])) { ?>
                                                                                
                                            <div class="form-group">
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong><?php echo $data['message_success'] ?></strong>
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>

                                        <?php
                                        } 
                                        ?>

                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address..." name="mail">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password" name="password">
                                        </div>
                                        <input class="btn btn-primary btn-user btn-block" type="submit" value="Connexion">
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="?controller=User&action=registration">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <?php include('./view/front/footer.html'); ?>

</body>

</html>
