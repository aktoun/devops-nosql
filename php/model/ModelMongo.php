
<?php
class ModelMongo
{

    /*
     * Attribut contenant l'instance PDO
     */
    private $manager;

    /*
     * Attribut statique qui contiendra l'unique instance de Model
     */
    private static $instance = null;

    /*
     * Constructeur : effectue la connexion à la base de données
     */
    private function __construct()
    {
        try {
            $this->manager = new MongoDB\Driver\Manager('mongodb://user:pass@mongoDB:27017');                    
        } catch (\MongoDB\Driver\Exception\Exception $e) {
            die('Echec connexion, erreur n°' . $e->getCode() . ':' . $e->getMessage());
        }
    }

    /*
     * Methode permettant de récupérer un modèle car le constructeur est privé
     */
    public static function getModel()
    {
        if (is_null(self::$instance)) {
            self::$instance = new ModelMongo();
        }
        return self::$instance;
    }//permet de ne pas recréer une instance si on en a déjà incrémenté une

//    public function getAllTrains() {
//        # setting your options and filter
//        $filter  = [];
//        $options = ['sort'=>array('_id'=>-1)]; # limit -1 from newest to oldest
//
//        #constructing the querry
//        $query = new MongoDB\Driver\Query($filter, $options);
//
//        #executing
//        $cursor = $this->manager->executeQuery('noSQL.Trains', $query);
        //var_dump($cursor);
//        return $cursor;
//    }
    
    public function getAllTrains($dateD, $quantiteDMD) {
        
        $filter = ["DateD" => [ '$gt' => $dateD], "Infos.Capacite" => [ '$gt' => $quantiteDMD]];
        $options = [];

        #constructing the querry
        $query = new MongoDB\Driver\Query($filter, $options);

        #executing
        $cursor = $this->manager->executeQuery('noSQL.Trains', $query)->toArray();
        return $cursor;
    }

    public function getTrainsParVilles($villeD, $villeA, $dateD, $quantiteDMD) {
        
        $filter = ["GareD.Ville" => $villeD, "GareA.Ville" => $villeA, "DateD" => [ '$gt' => $dateD], "Infos.Capacite" => [ '$gt' => $quantiteDMD]];
        $options = [];

        #constructing the querry
        $query = new MongoDB\Driver\Query($filter, $options);

        #executing
        $cursor = $this->manager->executeQuery('noSQL.Trains', $query)->toArray();
        return $cursor;
    }

    public function getTrainsParID($id) {

        $filter = ["id" => $id];
        $options = [];

        #constructing the querry
        $query = new MongoDB\Driver\Query($filter, $options);

        #executing
        $cursor = $this->manager->executeQuery('noSQL.Trains', $query);

        return $cursor;
    }

    public function updateCapacite($id,$quantite) {

        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(['id' => $id],['$set'=> ["Infos.Capacite"=> $quantite]]);
        #executing
        $this->manager->executeBulkWrite('noSQL.Trains',$bulk);
        //var_dump($cursor);
    }

    public function ajouterReservation($id, $nom, $prenom, $email,$nbrReservation) {

        $bulk = new MongoDB\Driver\BulkWrite;
        //TODO décider si addToSet ou Push, soit on ajoute tout le temps, soit jamais de doublons
        $bulk->update(['id' => $id], array('$push' => array("Reservations" => array("Nom" => $nom, "Prenom" => $prenom,"Email"=>$email, "NombreDePlaces"=>$nbrReservation))));

        $this->manager->executeBulkWrite('noSQL.Trains',$bulk);
    }

    public function getReservations($nom,$prenom, $email) {
        $filter = ["Reservations.Nom" => $nom,"Reservations.Prenom" => $prenom, "Reservations.Email" => $email];
        $options = [];

        #constructing the querry
        $query = new MongoDB\Driver\Query($filter, $options);

        #executing
        $cursor = $this->manager->executeQuery('noSQL.Trains', $query)->toArray();
        //var_dump($cursor);
        return $cursor;
    }

}
