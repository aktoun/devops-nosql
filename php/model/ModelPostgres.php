
<?php
class ModelPostgres
{

    /*
     * Attribut contenant l'instance PDO
     */
    private $bd;

    /*
     * Attribut statique qui contiendra l'unique instance de Model
     */
    private static $instance = null;

    /*
     * Constructeur : effectue la connexion à la base de données
     */
    private function __construct()
    {
        try {
            $dbName = "postgres";
            $dbUser = "postgres";
            $dbPassword = "postgres";
            $this->bd = new PDO("pgsql:host=postgres user=$dbUser dbname=$dbName password=$dbPassword");
            $this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->bd->query("SET names 'utf-8'");

        } catch (PDOException $e) {
            die('Echec connexion, erreur n°' . $e->getCode() . ':' . $e->getMessage());
        }
    }

    /*
     * Methode permettant de récupérer un modèle car le constructeur est privé
     */
    public static function getModel()
    {
        if (is_null(self::$instance)) {
            self::$instance = new ModelPostgres();
        }
        return self::$instance;
    }//permet de ne pas recréer une instance si on en a déjà incrémenté une

    /*Section Connexion / Inscription*/
    public function findUserByMail($mail) {
        try {
            $req = $this->bd->prepare('select * from "utilisateur" where email = :mail');
            $req->bindValue(":mail", $mail, PDO::PARAM_STR);//sécurité pour le paramètre mail
            $req->execute();
            return $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e){
            die('Erreur findUserByMail :' . $e->getMessage());
        }
    }

    public function userMailExist($mail){
        try {
            $req = $this->bd->prepare('select id from "utilisateur" where email = :mail');
            $req->bindValue(":mail", $mail, PDO::PARAM_STR);//binding mail input
            $req->execute();
            return $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e){
            die('Erreur userMailExist :' . $e->getMessage());
        }
    }

    public function createUser($mail, $mdp, $nom, $prenom){
        try{
            $req = $this->bd->prepare('INSERT INTO "utilisateur" (nom, prenom, email, password) VALUES (:nom, :prenom, :mail, :mdp)');
            $req->bindValue(':mail', $mail, PDO::PARAM_STR);
            $req->bindValue(':mdp', $mdp, PDO::PARAM_STR);
            $req->bindValue(':nom', $nom, PDO::PARAM_STR);
            $req->bindValue(':prenom', $prenom, PDO::PARAM_STR);
            $req->execute();
        }
        catch (PDOException $e){
            die('Erreur create user :' . $e->getMessage());
        }
    }

}
