<?php
    class ControllerTrains extends Controller
    {
        /**
         * Action par défaut du contrôleur (à définir dans les classes filles)
         */
        public function default()
        {
            $this->search();
        }

        public function search() 
        {
            $mod = ModelMongo::getModel();
            $villeD = htmlspecialchars($_POST['depart']);
            $villeA = htmlspecialchars($_POST['destination']);
            $dateD = htmlspecialchars($_POST['dateDepart']);
            $quantite = intval(htmlspecialchars($_POST['quantite']));

            if(($villeD == '') && ($villeA == '')) {
                $req = $mod->getAllTrains($dateD,$quantite);
            } 
            else 
            {
                $req = $mod->getTrainsParVilles($villeD,$villeA,$dateD,$quantite);
            }
            $res = [];
            
            if(count($req)>0)
            {
                //var_dump($req);
                foreach ($req as $tuple) {
                    $trajet = [];
                    $trajet["id"] = $tuple->id;
                    $trajet["DateD"] = $tuple->DateD;
                    $trajet["GareD"] = $tuple->GareD->Ville;
                    $trajet["GareA"] = $tuple->GareA->Ville;
                    $trajet["Capacite"] = $tuple->Infos->Capacite;
                    $trajet["quantite"] = $quantite;
                    array_push($res, $trajet);
                }
                
                $_SESSION['res'] = json_encode($res);
                //echo ($SESSION['res']);
                $this->render("resultat", []);
            }
            else
            {
                $data["message_error"] = "Aucun résultat selon vos critères.";
                $this->render("home", $data);
            }
        }

        public function reserver()
        {
            $mod = ModelMongo::getModel();
            $idTrain = intval(htmlspecialchars($_POST['id']));
            $nbrReservation = intval(htmlspecialchars($_POST['nbrReservation']));
            $nom = $_SESSION['user']['nom'];
            $prenom = $_SESSION['user']['prenom'];
            $email = $_SESSION['user']['email'];

            $capacite = 0;

            $req = $mod->getTrainsParID($idTrain);
            foreach ($req as $tuple) {
                $capacite = $tuple->Infos->Capacite;
            }

            $mod->updateCapacite($idTrain,$capacite - $nbrReservation);

            $mod->ajouterReservation($idTrain,$nom,$prenom,$email,$nbrReservation);

            $data["message_success"] = "Réservation faite !";
            $this->render("home", $data);
        }

        public function AfficherReservations()
        {
            $mod = ModelMongo::getModel();
            $req = $mod->getReservations($_SESSION['user']['nom'],$_SESSION['user']['prenom'],$_SESSION['user']['email']);

            $res = [];
            
            if(count($req)>0)
            {
                //var_dump($req);
                foreach ($req as $tuple) {
                    $trajet = [];
                    $trajet["id"] = $tuple->id;
                    $trajet["DateD"] = $tuple->DateD;
                    $trajet["GareD"] = $tuple->GareD->Ville;
                    $trajet["GareA"] = $tuple->GareA->Ville;
                    $trajet["Capacite"] = $tuple->Infos->Capacite;

                    $somme = 0;
                    foreach($tuple->Reservations as $passager) {
                        if($passager->Email == $_SESSION['user']['email']) {
                            $somme = $somme + $passager->NombreDePlaces;
                        }
                        $trajet["placeReserve"] = $somme;
                    }
                    array_push($res, $trajet);
                }

                //var_dump($res);
                
                $_SESSION['reservation'] = json_encode($res);
                $this->render("reservation", []);
            }
            else
            {
                $data["message_error"] = "Aucune réservation faite";
                $this->render("home", $data);
            }
        }

    }
?>