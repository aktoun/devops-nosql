<?php
    class ControllerUser extends Controller
    {

        /**
         * Action par défaut du contrôleur (à définir dans les classes filles)
         */
        public function default()
        {
            $this->login();
        }

        public function login() 
        {
            $data = [];
            if(isset($_SESSION['user'])) {
                $this->render("home", $data);
            }
            
            if (!isset($_POST["mail"]) AND !isset($_POST["password"])) //On arrive de base et on a pas utilisé le formulaire
            {
                $this->render("login", $data);
            }
            else 
            {  //on a utilisé le formulaire
                $mail = htmlspecialchars($_POST['mail']);
                $password = htmlspecialchars($_POST['password']);
                
                $mod = ModelPostgres::getModel();
                $req = $mod->findUserByMail(htmlspecialchars($mail)); //sécurité sur l'input de l'utilisateur
                
                //var_dump($req);
                
                if(!$req) //pas d'utilisateur avec ce mail
                {
                    $data = ['e-message' => "E-mail incorrect !"];
                    $this->render("login", $data);
                }
                else 
                {
                    if (password_verify((htmlspecialchars($password)), $req['password'])) //vérification du mot de passe crypté
                    {
                        $_SESSION['user'] = $req;
                        $this->render("home", $data);
                    } 
                    else 
                    {
                        $data = ['e-message' => "Mot de passe incorrect !"];
                        $this->render("login", $data);
                    }
                }
                
            }

            $this->render("home", $data);
        }

        public function logout() 
        {
            $data = [];
            session_destroy();
            $this->render("login", $data);
        }

        /**
         * Fonction qui permet à un utilisateur de s'inscrire à notre application
         */
        public function registration(){

            $data = [];
            if (!isset($_POST["nom"]) AND !isset($_POST["prenom"]) AND !isset($_POST["mail"]) AND !isset($_POST["password"])) //On arrive de base et on a pas utilisé le formulaire
            {
                $this->render("register", $data);
            }

            $pass = htmlspecialchars($_POST["password"]);
            $mail = htmlspecialchars($_POST["mail"]);
            $nom = htmlspecialchars($_POST["nom"]);
            $prenom = htmlspecialchars($_POST["prenom"]);

            $data = [];
            $mod = ModelPostgres::getModel();

            $req = $mod->userMailExist($mail);
            if( $req != false){//si le mail existe la fct renvoit la requete sql si le mail n'existe pas la requete sql échoue et renvoit false
                $data["message_err"] = "Erreurs mail dejà attribuée";
                $this->render("register", $data);
            }

            $mdp_crypt = password_hash($pass, PASSWORD_DEFAULT);//hasher le mot de passe pour le stocker crypté dans la BD
            $mod->createUser($mail, $mdp_crypt, $nom, $prenom);
            $data["message_success"] = "Utilisateur ajouté avec succès";
            $this->render("login", $data);
        }

    }
?>